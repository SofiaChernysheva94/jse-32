package ru.t1.chernysheva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.api.service.IAuthService;
import ru.t1.chernysheva.tm.enumerated.Role;
import ru.t1.chernysheva.tm.model.User;
import ru.t1.chernysheva.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractUserCommand{

    @NotNull
    private final String NAME = "user-registry";

    @NotNull
    private final String DESCRIPTION = "User registry.";

    @NotNull
    @Override
    public String getName() {
        return  NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return  DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("[ENTER LOGIN: ]");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD: ]");
        @Nullable final String password = TerminalUtil.nextLine();
        @Nullable final IAuthService authService = getAuthService();
        @Nullable final User user = authService.registry(login, password);
        showUser(user);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
