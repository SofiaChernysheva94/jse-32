package ru.t1.chernysheva.tm.command;

import ru.t1.chernysheva.tm.api.model.ICommand;
import ru.t1.chernysheva.tm.api.service.IAuthService;
import ru.t1.chernysheva.tm.api.service.IPropertyService;
import ru.t1.chernysheva.tm.api.service.IServiceLocator;
import ru.t1.chernysheva.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }

    public String getUserId() {
        return getAuthService().getUserId();
    }

    public abstract Role[] getRoles();

    public abstract String getArgument();

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        result += name + " : ";
        if (argument != null) result += argument + " : ";
        result += description;
        return result;
    }

}
