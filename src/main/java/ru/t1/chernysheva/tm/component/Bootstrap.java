package ru.t1.chernysheva.tm.component;

import lombok.*;
import org.jetbrains.annotations.*;
import org.reflections.Reflections;
import ru.t1.chernysheva.tm.api.endpoint.ISystemEndpoint;
import ru.t1.chernysheva.tm.api.repository.ICommandRepository;
import ru.t1.chernysheva.tm.api.repository.IProjectRepository;
import ru.t1.chernysheva.tm.api.repository.ITaskRepository;
import ru.t1.chernysheva.tm.api.repository.IUserRepository;
import ru.t1.chernysheva.tm.api.service.*;
import ru.t1.chernysheva.tm.command.AbstractCommand;
import ru.t1.chernysheva.tm.command.data.AbstractDataCommand;
import ru.t1.chernysheva.tm.command.data.DataBase64LoadCommand;
import ru.t1.chernysheva.tm.command.data.DataBinaryLoadCommand;
import ru.t1.chernysheva.tm.dto.request.ServerAboutRequest;
import ru.t1.chernysheva.tm.dto.request.ServerVersionRequest;
import ru.t1.chernysheva.tm.endpoint.SystemEndpoint;
import ru.t1.chernysheva.tm.enumerated.Role;
import ru.t1.chernysheva.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.chernysheva.tm.exception.system.CommandNotSupportedException;
import ru.t1.chernysheva.tm.model.User;
import ru.t1.chernysheva.tm.repository.CommandRepository;
import ru.t1.chernysheva.tm.repository.ProjectRepository;
import ru.t1.chernysheva.tm.repository.TaskRepository;
import ru.t1.chernysheva.tm.repository.UserRepository;
import ru.t1.chernysheva.tm.service.*;
import ru.t1.chernysheva.tm.util.SystemUtil;
import ru.t1.chernysheva.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.chernysheva.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService(propertyService);

    @NonNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository,
                                                             projectRepository,
                                                             taskRepository,
                                                             propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
            reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);

    }

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private void processArgument(@Nullable final String argument) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private  void initBackup() {
        backup.start();
    }

    private void prepareStartup() {
        initPID();
        initDemoData();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
        fileScanner.start();
        server.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        backup.stop();
        fileScanner.stop();
        server.stop();
    }

    private void initData() {
        final boolean checkBinary = Files.exists(Paths.get(AbstractDataCommand.FILE_BINARY));
        if (checkBinary) processCommand(DataBinaryLoadCommand.NAME, false);
        if (checkBinary) return;
        final boolean checkBase64 = Files.exists(Paths.get(AbstractDataCommand.FILE_BASE64));
        if (checkBase64) processCommand(DataBase64LoadCommand.NAME, false);
    }

    @SuppressWarnings("primitive types")
    private boolean processArgument(@Nullable final String[] arguments) {
        if (arguments == null || arguments.length < 1) return false;
        processArgument(arguments[0]);
        return true;
    }

    void processCommand(@Nullable final String command, boolean checkRoles) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        if (checkRoles) authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    public void processCommand(@Nullable final String command) {
        processCommand(command, true);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (command == null) throw new CommandNotSupportedException();
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    public void run(final String[] args) {
        if (processArgument(args)) System.exit(0);

        prepareStartup();
        while (!Thread.currentThread().isInterrupted()) initCommands();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData()  {
        @NotNull final User admin = userService.create("admin", "admin", Role.ADMIN);
        @NotNull final User test = userService.create("test", "test");

        projectService.create(admin.getId(), "Project1", "Description1");
        projectService.create(admin.getId(), "Project2", "Description2");

        projectService.create(test.getId(), "Project1", "Description1");
        projectService.create(test.getId(), "Project2", "Description2");

        taskService.create(admin.getId(), "Task1", "Description1");
        taskService.create(admin.getId(), "Task2", "Description2");
    }

}
