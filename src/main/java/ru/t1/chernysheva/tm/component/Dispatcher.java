package ru.t1.chernysheva.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.t1.chernysheva.tm.dto.request.AbstractRequest;
import ru.t1.chernysheva.tm.dto.response.AbstractResponse;
import ru.t1.chernysheva.tm.api.endpoint.Operation;

import java.util.LinkedHashMap;
import java.util.Map;

public class Dispatcher {

    @NotNull final Map<Class<? extends AbstractRequest>, Operation<?, ?>> map = new LinkedHashMap<>();

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            @NotNull final Class<RQ> reqClass, @NotNull final Operation<RQ, RS> operation
    ) {
        map.put(reqClass, operation);
    }


    @NotNull
    @SuppressWarnings({"unchecked","rawtypes"})
    public Object call(@NotNull final AbstractRequest request) {
        @Nullable final Operation operation = map.get(request.getClass());
        if (operation == null) throw new RuntimeException(request.getClass().getName());
        return operation.execute(request);
    }

}
