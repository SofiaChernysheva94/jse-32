package ru.t1.chernysheva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.enumerated.Role;
import ru.t1.chernysheva.tm.model.User;

public interface IAuthService {

    @Nullable
    User registry(@Nullable String login, @Nullable String password);

    void login(@Nullable String login, @Nullable String password);

    void logout();

    boolean isAuth();

    @Nullable
    String getUserId();

    @NotNull
    User getUser();

    void checkRoles(final Role[] roles);

}
