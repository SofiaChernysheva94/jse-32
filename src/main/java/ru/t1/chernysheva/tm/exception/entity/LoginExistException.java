package ru.t1.chernysheva.tm.exception.entity;

public class LoginExistException extends AbstractEntityException {

    public LoginExistException() {
        super("Error! Login is not exist...");
    }

}
