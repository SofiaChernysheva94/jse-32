package ru.t1.chernysheva.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.api.service.IPropertyService;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String GIT_BRANCH = "gitBranch";

    @NotNull
    public static final String GIT_COMMIT_ID = "gitCommitId";

    @NotNull
    public static final String GIT_COMMITTER_NAME = "gitCommitterName";

    @NotNull
    public static final String GIT_COMMITTER_EMAIL = "gitCommitterEmail";

    @NotNull
    public static final String GIT_COMMIT_MESSAGE = "gitCommitMessage";

    @NotNull
    public static final String GIT_COMMIT_TIME = "gitCommitTime";

    @NotNull
    public static final String APPLICATION_NAME_KEY = "application.name";

    @NotNull
    public static final String APPLICATION_NAME_DEFAULT = "tm";

    @NotNull
    public static final String APPLICATION_FILE_NAME_KEY = "application.config";

    @NotNull
    public static final String APPLICATION_FILE_NAME_DEFAULT = "src/main/assembly/release/config/application.properties";

    @NotNull
    public static final String APPLICATION_LOG_KEY = "application.log";

    @NotNull
    public static final String APPLICATION_LOG_DEFAULT = "./";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "25456";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "3568548474";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    public static final String APPLICATION_SERVER_PORT = "server.port";

    @NotNull
    private static final String EMPTY_VALUE = "---";


    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        final boolean existsConfig = isExistsExternalConfig();
        if (existsConfig) loadExternalConfig(properties);
        else loadInternalConfig(properties);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return "1.32";
        //return read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getGitBranch() {
        return read(GIT_BRANCH);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationConfig() {
        return getStringValue(APPLICATION_FILE_NAME_KEY, APPLICATION_FILE_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationLog() {
        return getStringValue(APPLICATION_LOG_KEY, APPLICATION_LOG_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        return getIntegerValue(APPLICATION_SERVER_PORT);
    }

    @NotNull
    @Override
    public String getApplicationName() {
        return getStringValue(APPLICATION_NAME_KEY, APPLICATION_NAME_DEFAULT);
    }

    @SneakyThrows
    private void loadInternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = APPLICATION_FILE_NAME_DEFAULT;
        @Cleanup @Nullable final InputStream inputStream = getSystemResourceAsStream(name);
        if (inputStream == null) return;
        properties.load(inputStream);
    }

    @SneakyThrows
    private void loadExternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        @Cleanup @NotNull final InputStream inputStream = new FileInputStream(file);
        properties.load(inputStream);
    }

    private boolean isExistsExternalConfig() {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        return file.exists();
    }

    @NotNull
    private Integer getIntegerValue(@NotNull final String key) {
        return Integer.parseInt(getStringValue(key, PropertyService.PASSWORD_ITERATION_DEFAULT));
    }

    @NotNull
    private String read(@Nullable final String key){
        if (key == null || key.isEmpty()) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    @Override
    public String getGitCommitId() {
        return read(GIT_COMMIT_ID);
    }

    @NotNull
    @Override
    public String getGitCommitterName() {
        return read(GIT_COMMITTER_NAME);
    }

    @NotNull
    @Override
    public String getGitCommitterEmail() {
        return read(GIT_COMMITTER_EMAIL);
    }

    @NotNull
    @Override
    public String getGitCommitMessage() {
        return read(GIT_COMMIT_MESSAGE);
    }

    @NotNull
    @Override
    public String getGitCommitTime() {
        return read(GIT_COMMIT_TIME);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".","_");
    }

}
